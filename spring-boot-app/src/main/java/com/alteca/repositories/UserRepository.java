package com.alteca.repositories;

import com.alteca.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByIdKeycloak(String idKeycloak);
}
