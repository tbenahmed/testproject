package com.alteca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventAppMain {

    public static void main(String[] args) {
        SpringApplication.run(EventAppMain.class, args);
    }
}
