package com.alteca.controllers;

import java.util.List;

import com.alteca.models.Event;
import com.alteca.services.EventService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;




@RestController
@RequestMapping("/api")
public class EventController {

    private final EventService eventService;

    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping("/index/events")
    public ResponseEntity<Object> findAll() {
        try {
            List<Event> events = this.eventService.findAll();
            return ResponseEntity.ok(events);
        } catch (Exception e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }

    @GetMapping("/event/{idEvent}")
    public ResponseEntity<Object> findOne(@PathVariable long idEvent) {
        try {
            Event event = this.eventService.findById(idEvent);

            if (event == null) {
                return ResponseEntity.ok("No event found");
            }
            return ResponseEntity.ok(event);
        } catch (Exception e) {
            return ResponseEntity.status(500).body("error");
        }
    }

    @PostMapping("/admin/event/add")
    public ResponseEntity<Object> addEvent(@RequestBody Event event) {
        try {
            System.out.println(event.getName());
            Event eventSaved = this.eventService.save(event);
            return ResponseEntity.ok(eventSaved);
        } catch (Exception e) {
            return ResponseEntity.status(500).body(e.getMessage());
        }
    }
}