package com.alteca.controllers;

import com.alteca.models.User;
import com.alteca.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api")
public class UserController {

    private final UserService userService;
    private final HttpServletRequest request;

    public UserController(UserService userService, HttpServletRequest request) {
        this.userService = userService;
        this.request = request;
    }

    @GetMapping("/user/{idUser}")
    public ResponseEntity<Object> findOne(@PathVariable long idUser) {
        try {

            User user = this.userService.findById(idUser);

            if (user == null) {
                return ResponseEntity.ok("No user found");
            }
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            return ResponseEntity.status(500).body("error");
        }
    }

    @GetMapping("/user/auth")
    public ResponseEntity<User> getLoggedUser(Principal principal, User user) {
        return ResponseEntity.ok(this.userService.getUser(principal, user));
    }
}
