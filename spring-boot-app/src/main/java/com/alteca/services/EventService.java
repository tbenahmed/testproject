package com.alteca.services;


import com.alteca.models.Event;

import java.util.List;

public interface EventService {

    List<Event> findAll();

    Event findById(long id);

    Event save(Event event);
}
