package com.alteca.services.impl;

import com.alteca.models.User;
import com.alteca.repositories.UserRepository;
import com.alteca.services.UserService;
import com.alteca.utils.KeycloakUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userDAO;
    private final KeycloakUserMapper keycloakUserMapper;

    @Autowired
    public UserServiceImpl(UserRepository userDAO, KeycloakUserMapper keycloakUserMapper) {
        this.userDAO = userDAO;
        this.keycloakUserMapper = keycloakUserMapper;
    }

    @Override
    public List<User> findAll() {
        return this.userDAO.findAll();
    }

    @Override
    public User findById(long id) {
        return this.userDAO.findById(id).orElse(null);
    }

    @Override
    public User save(User user) {
        return this.userDAO.save(user);
    }

    @Override
    public User findByIdKeycloak(String keycloakId) {
        return this.userDAO.findByIdKeycloak(keycloakId);
    }

    @Override
    public User getUser(Principal userPrincipal, User user) {
        String keycloakId = keycloakUserMapper.getIdKeycloak(userPrincipal);
        return isUserExists(keycloakId)
                ? findByIdKeycloak(keycloakId)
                : save(keycloakUserMapper.getUserFromPrincipal(userPrincipal, user));
    }

    @Override
    public boolean isUserExists(String keycloakId) {
        return this.userDAO.findByIdKeycloak(keycloakId) != null;
    }

}
