package com.alteca.services.impl;

import com.alteca.models.Event;
import com.alteca.repositories.EventRepository;
import com.alteca.services.EventService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    private final EventRepository eventDAO;

    public EventServiceImpl(EventRepository eventDAO) {
        this.eventDAO = eventDAO;
    }

    @Override
    public List<Event> findAll() {
        return this.eventDAO.findAll();
    }

    @Override
    public Event findById(long id) {
        return this.eventDAO.findById(id).orElse(null);
    }

    @Override
    public Event save(Event event) {
        return this.eventDAO.save(event);
    }

}