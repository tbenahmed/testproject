package com.alteca.services;

import com.alteca.models.User;

import java.security.Principal;
import java.util.List;

public interface UserService {

    List<User> findAll();

    User findById(long id);

    User save(User user);

    User findByIdKeycloak(String keycloakId);

    User getUser(Principal userPrincipal, User user);

    boolean isUserExists(String keycloakId);
}