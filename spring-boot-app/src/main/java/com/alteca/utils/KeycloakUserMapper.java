package com.alteca.utils;

import com.alteca.models.User;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.stereotype.Component;

import java.security.Principal;

@Component
public class KeycloakUserMapper {

    private AccessToken getAccessToken(Principal principal) {
        KeycloakAuthenticationToken authenticationToken = (KeycloakAuthenticationToken) principal;
        return authenticationToken.getAccount().getKeycloakSecurityContext().getToken();
    }

    public String getIdKeycloak(Principal principal) {
        return this.getAccessToken(principal).getSubject();
    }

    public User getUserFromPrincipal(Principal principal, User user) {
        AccessToken accessToken = this.getAccessToken(principal);
        user.setIdKeycloak(accessToken.getSubject());
        user.setEmail(accessToken.getEmail());
        user.setUserName(accessToken.getPreferredUsername());
        user.setFirstName(accessToken.getGivenName());
        user.setLastName(accessToken.getFamilyName());
        return user;
    }
}
